package meals.MpesaPush;

import java.util.Objects;

public class UserData 
{
	String phone = "254728626789";
	
	public UserData() {}
	
	public UserData(String phone)
	{
		this.phone = Objects.requireNonNull(getPhone());
	}
	
	public void setPhone(String phone) 
	{
		this.phone = phone;
		System.out.println("2. Phone number set to " + getPhone());
	}
	public String getPhone() 
	{
		System.out.println("1. Phone number: " + phone);
		return phone;
		
	}
}
