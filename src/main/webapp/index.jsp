<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>

table, th, td 
	{
		border: 1px solid black;
		border-collapse: collapse;
		font-size:30px;
		
	}
	/*body
	{
		background-image:url(meals.jpeg)
		filter: blur(8px);
	}*/
	th, td 
	{
		padding: 5px;
		text-align: left;
	}


body, html {
  height: 100%;
  margin: 0;
  font-family: Arial, Helvetica, sans-serif;
}

* {
  box-sizing: border-box;
}

.bg-image {
  /* The image used */
  background-image: url("meals.jpeg");
  
  /* Add the blur effect */
  filter: blur(8px);
  -webkit-filter: blur(8px);
  
  /* Full height */
  height: 100%; 
  
  /* Center and scale the image nicely */
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
}

/* Position text in the middle of the page/image */
.bg-text {
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0, 0.4); /* Black w/opacity/see-through */
  color: white;
  font-weight: bold;
  border: 3px solid #f1f1f1;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  z-index: 2;
  width: 80%;
  padding: 20px;
  text-align: center;
}

.button {
  background-color: #9ACD32; /* Green */
  border: none;
  color: white;
  padding: 20px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 25px;
  margin: 4px 2px;
  cursor: pointer;
  width: 250px;
}
.button4 {border-radius: 12px;}

</style>
</head>
<body>

<div class="bg-image"></div>

<div class="bg-text">
  
<h1>Welcome to Mtugo, 
we have a wide array of meals to choose from, please proceed to select  a meal of your choosing then pay 
via <i>M-pesa (Yeah, its here now!)</i>, get to the hotel when your meal is ready for picking


... no more queueing!!!</h1>
<form action="meals.jsp">

<button class="button button4">View meals</button>
</form>



</div>

</body>
</html>
